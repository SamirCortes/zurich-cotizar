import StepForm from '../shared/StepForm/StepForm';
import { useNavigate } from 'react-router-dom';

import CoverageGroup from '../shared/CoverageGroup/CoverageGroup';
import ConfigProductForm from '../shared/ConfigProductForm/ConfigProductForm';

//Icons
import IconTitle from '../assets/icons/List_points.svg';

const PlanConfigPage = () => {
  const navigate = useNavigate();

  return (
    <div className="Page">
      <StepForm step={1} title="Cotiza" />
      <div className="PageContent">
        <div className="PageTop">
          <img className="IconTitle" src={IconTitle} alt="" />
          <h1>Configurar un plan</h1>
        </div>
        <section className="Section">
          <CoverageGroup />
          <button className="btn btn-primary mb-2">
            Agregar y/o eliminar coberturas
          </button>
        </section>
        <hr className="lineSeparation" />
        <ConfigProductForm />
      </div>
      <div className="PageActions">
        {/* <z-button config="primary">Continuar</z-button> */}
        <button
          className="btn btn-outline"
          onClick={() => navigate('/configurar-producto')}
        >
          Volver
        </button>
        <button
          className="btn btn-negative"
          onClick={() => navigate('/modalidad-pago')}
        >
          Contratar
        </button>
      </div>
    </div>
  );
};

export default PlanConfigPage;
