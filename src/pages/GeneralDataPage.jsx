import { useNavigate } from 'react-router-dom';
import StepForm from '../shared/StepForm/StepForm';

//Icons
import IconTitle from '../assets/icons/GeneralData-title.svg';

const GeneralDataPage = () => {
  const navigate = useNavigate();
  const redirectNextPage = () => navigate('/datos-hogar');

  return (
    <div className="Page">
      <StepForm step={1} title="Cotiza" />
      <div className="PageContent">
        <div className="d-flex align-items-center">
          <img className="IconTitle" src={IconTitle} alt="" />
          <h1>Datos generales</h1>
        </div>
        <div className="d-flex flex-column p-4">
          <label z-text-input="shaped" icon="card-closed:outline">
            <span>RUT</span>
            <input type="text" placeholder="" />
          </label>
          <label
            z-text-input="shaped"
            icon="mail-closed:outline"
            className="mt-4"
          >
            <span>Email</span>
            <input type="text" placeholder="" />
          </label>
          <label
            z-number-input="shaped"
            icon="mobile-closed:outline"
            className="mt-4"
          >
            <span>Celular</span>
            <small>Ej. +56912345678</small>
            <input type="number" placeholder="" />
          </label>
          <label z-checkbox className='mt-3'>
            <input type="checkbox" />
            <span className='SubtitleSm ml-2'>Autorizo el uso de mis datos para futuros contactos según la<span className='SubtitleSm__checkboxLink'> política de privacidad</span></span>
          </label>
        </div>
      </div>
      <div className="PageActions">
        {/* <z-button config="primary">Continuar</z-button> */}
        <button className="btn btn-negative" onClick={redirectNextPage}>
          Continuar
        </button>
      </div>
    </div>
  );
};

export default GeneralDataPage;
