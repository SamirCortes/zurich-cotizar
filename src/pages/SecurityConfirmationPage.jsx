import StepForm from '../shared/StepForm/StepForm';
import { useNavigate } from 'react-router-dom';

//Icons
import AlertIcon from '../assets/icons/AlertTriangleSecurityPage.svg';

const SecurityConfirmationPage = () => {
  const navigate = useNavigate();
  const redirectNextPage = () => navigate('/inspeccionar');
  return (
    <div className="Page">
      <StepForm step={2} title="Contrata" />
      <div className="PageContent">
        <div className="d-flex align-items-center">
          <img className="IconTitle mr-2" src={AlertIcon} alt="" />
          <h1>Confirmacion de seguridad</h1>
        </div>
        <section className="Section">
          <p className="Subtitle">
            Elige un método para confirmar tu identidad y poder firmar los
            contratos
          </p>
        </section>
        <section className="Section pl-3">
        <z-radio-select
          className='d-flex gap-4'
          value="Si"
          name="radio-select"
          options='[{ "text": "Número de serie de tu carnet", "value": "Si"}, { "text": "Código de verificacion vía email", "value": "No"}]'
        ></z-radio-select>
        </section>
      </div>
      <div className="PageActions">
        {/* <z-button config="primary">Continuar</z-button> */}
        <button className="btn btn-outline" onClick={() => navigate('/contratos')}>
          Volver
        </button>
        <button className="btn btn-negative" onClick={redirectNextPage}>
          Continuar
        </button>
      </div>
    </div>
  );
};

export default SecurityConfirmationPage;
