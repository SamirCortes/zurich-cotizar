import StepForm from '../shared/StepForm/StepForm';
import { useNavigate } from 'react-router-dom';
import Toast from '../shared/Toast/Toast'

//Icons
import AlertIcon from '../assets/icons/AlertClientStatusPage.svg';

const ClientStatusPage = () => {
  const navigate = useNavigate();
  const redirectNextPage = () => navigate('/contratos');

  return (
    <div className="Page">
      <StepForm step={2} title="Contrata" />
      <div className="PageContent">
        <div className="d-flex align-items-center">
          <img className="IconTitle" src={AlertIcon} alt="" />
          <h1>Estado de cliente</h1>
        </div>
        <section className='Section'>
        <Toast title={'Pago realizado'} message={'Fecha de pago 12/03/2024'}/>
        </section>
        <section className="Section">
          <p className="SubtitleMd">Reenviar link de pago y contratoss</p>
          <p className="Text">
            Confirma los datos para enviar el link de pago y contratos
          </p>
          <label z-text-input="shaped" icon="mail-closed:outline">
            <span>Email</span>
            <input type="text" placeholder="" />
          </label>
          <button className="btn btn-primary mt-2 w-50">Reenviar</button>
        </section>
      </div>
      <div className="PageActions">
        {/* <z-button config="primary">Continuar</z-button> */}
        <button
          className="btn btn-outline"
          onClick={() => navigate('/link-pago')}
        >
          Volver
        </button>
        <button className="btn btn-negative" onClick={redirectNextPage}>
          Continuar
        </button>
      </div>
    </div>
  );
};

export default ClientStatusPage;
