import { useNavigate } from 'react-router-dom';
import StepForm from '../shared/StepForm/StepForm';
import PaymentMethodCard from '../shared/PaymentMethodCard/PaymentMethodCard';
import DataOwnerCard from '../shared/DataOwnerCard/DataOwnerCard';
import Toast from '../shared/Toast/Toast';
import SureExtraPaymentMethodPage from '../shared/SureExtraPaymentMethodPage/SureExtraPaymentMethodPage';

//Icons
import IconTitle from '../assets/icons/PaymentMethod-title.svg';

const PaymentMethodPage = () => {
  const navigate = useNavigate();
  const redirectNextPage = () => navigate('/medios-pago');

  return (
    <div className="Page">
      <StepForm step={2} title="Contrata" />
      <div className="PageContent">
        <div className="d-flex align-items-center">
          <img className="IconTitle" src={IconTitle} alt="" />
          <h1>Modalidad de pago</h1>
        </div>
        <DataOwnerCard />
        <PaymentMethodCard />
        <Toast
          message={
            'UF al valor del día de hoy. Tendrás 15 días para realizar la inspección de tu auto para que  tu póliza sea vigente.'
          }
        />
        <SureExtraPaymentMethodPage />
        <section className="Section">
          <p className="title">¿Cómo quieres pagar</p>
          <z-radio-select
            className="d-flex gap-4"
            value="Opcíon"
            name="radio-select"
            options='[{ "text": "Pago total al contado", "value": "Opcíon"},  { "text": "Pagar 1° cuota sin mandato asociado", "value": "Opcíon 2"},  { "text": "Pagar 1° cuota con enrolamiento", "value": "Opcíon 3"}, { "text": "PAC/PAT", "value": "Opcíon 4"}]'
          ></z-radio-select>
        </section>
      </div>
      <div className="PageActions">
        {/* <z-button config="primary">Continuar</z-button> */}
        <button
          className="btn btn-outline"
          onClick={() => navigate('/configurar-plan')}
        >
          Volver
        </button>
        <button className="btn btn-negative" onClick={redirectNextPage}>
          Continuar
        </button>
      </div>
    </div>
  );
};

export default PaymentMethodPage;
