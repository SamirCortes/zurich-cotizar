import { useNavigate } from 'react-router-dom';
import StepForm from '../shared/StepForm/StepForm';

//Icons
import IconTitle from '../assets/icons/MeanspaymentPageCreditcard.svg';
import IconWebPay from '../assets/icons/MeanspaymentPageWedPay.svg';
import IconMercadoPago from '../assets/icons/MeanspaymentPageMercadoPago.svg';
import IconOneClick from '../assets/icons/MeanspaymentPageOneClick.svg';
import IconFlow from '../assets/icons/MeanspaymentPageFlow.svg';

const MeanspaymentPage = () => {
  const navigate = useNavigate();
  const redirectNextPage = () => navigate('/contrato');

  return (
    <div className="Page">
      <StepForm step={2} title="Contrata" />
      <div className="PageContent">
        <div className="d-flex align-items-center">
          <img className="IconTitle" src={IconTitle} alt="" />
          <h1>Medios de pago</h1>
        </div>
        <section className='Section'>
             <p className="title">¿cómo quieres pagar?</p>
        </section>
       
        <div className="CardsOptions">
          <div className="CardsOptions__cardItem mb-4 mr-3">
            <img src={IconWebPay} alt="" />
          </div>
          <div className='CardsOptions__cardItem'>
          <img src={IconMercadoPago} alt="" />
          </div>
          <div className='CardsOptions__cardItem mr-3'>
          <img src={IconOneClick} alt="" />
          </div>
          <div className='CardsOptions__cardItem'>
          <img src={IconFlow} alt="" />
          </div>
        </div>
      </div>
      <div className="PageActions">
        {/* <z-button config="primary">Continuar</z-button> */}
        <button
          className="btn btn-outline"
          onClick={() => navigate('/modalidad-pago')}
        >
          Volver
        </button>
        <button className="btn btn-negative" onClick={redirectNextPage}>
          Continuar
        </button>
      </div>
    </div>
  );
};

export default MeanspaymentPage;
