import StepForm from '../shared/StepForm/StepForm';
import { useNavigate } from 'react-router-dom';

//Icons
import DocumentsIcon from '../assets/icons/ContractsDocumentsPage.svg';

const OnboardingContractsPage = () => {

    const navigate = useNavigate();
    const redirectNextPage = () => navigate('/link-pago');

  return (
    <div className="Page">
      <StepForm step={2} title="Contrata" />
      <div className="PageContent">
        <div className="PageContractsContainer">
          <img src={DocumentsIcon} alt="" />
          <div className="PageContractsContainer__content">
            <section className="Section">
              <p className="title mb-2">¿cómo quieres pagar?</p>
            </section>
            <p className='PageContractsContainer__text'>
              Entendemos que tu tiempo es valioso. Es por eso que hemos
              simplificado todo el proceso de contratación para que puedas
              obtener la protección que necesitas de manera rápida y sin
              complicaciones.<br/> <br/> Lee los siguientes contrato y firmalos para
              asegurar la contratación de tu póliza
            </p>
          </div>
        </div>
      </div>
      <div className="PageActions">
        {/* <z-button config="primary">Continuar</z-button> */}
        <button
          className="btn btn-outline"
          onClick={() => navigate('/medios-pago')}
        >
          Salir
        </button>
        <button className="btn btn-negative" onClick={redirectNextPage}>
          Ver contratos
        </button>
      </div>
    </div>
  );
};

export default OnboardingContractsPage;
