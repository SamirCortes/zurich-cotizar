import StepForm from "../shared/StepForm/StepForm";
import { useNavigate } from "react-router-dom";

//Icons
import LinkIcon from "../assets/icons/LinkPage.svg";

const PaymentLinkPage = () => {
  const navigate = useNavigate();
  const redirectNextPage = () => navigate("/estado-cliente");

  return (
    <div className="Page">
      <StepForm step={2} title="Contrata" />
      <div className="PageContent">
        <div className="d-flex align-items-center">
          <img className="IconTitle" src={LinkIcon} alt="" />
          <h1>Link</h1>
        </div>
        <section className="Section">
          <p className="SubtitleMd">Enviar link de pago y contratos</p>
          <p className="Text">
            Confirma los datos para enviar el link de pago y contratos
          </p>
          <label z-text-input="shaped" icon="mail-closed:outline" className="mt-3">
            <span>Email</span>
            <input type="text" placeholder="" />
          </label>
        </section>
      </div>
      <div className="PageActions">
        {/* <z-button config="primary">Continuar</z-button> */}
        <button
          className="btn btn-outline"
          onClick={() => navigate("/contrato")}
        >
          Volver
        </button>
        <button className="btn btn-negative" onClick={redirectNextPage}>
          Enviar
        </button>
      </div>
    </div>
  );
};

export default PaymentLinkPage;
