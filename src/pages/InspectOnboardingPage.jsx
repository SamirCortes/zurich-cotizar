import StepForm from '../shared/StepForm/StepForm';
import { useNavigate } from 'react-router-dom';

//Icons
import chekIcon from '../assets/icons/FinanceCheckInspectPage.svg';

const InspectOnboardingPage = () => {
  const navigate = useNavigate();
  const redirectNextPage = () => navigate('/realizar-inspeccion');

  return (
    <div className="Page">
      <StepForm step={2} title="Contrata" />
      <div className="PageContent">
        <div className="PageContractsContainer">
          <img src={chekIcon} alt="" />
          <div className="PageContractsContainer__content">
            <section className="Section">
              <p className="title mb-2">¡Ya casi estamos listos!</p>
            </section>
            <p className="PageContractsContainer__text">
              Ahora debes realizar la inspección de tu auto. Este paso es muy
              importante, pues <strong> la cobertura sólo se activará una vez que la
              inspección sea aprobada.</strong>
            </p>
          </div>
        </div>
      </div>
      <div className="PageActions">
        {/* <z-button config="primary">Continuar</z-button> */}
        <button
          className="btn btn-outline"
          onClick={() => navigate('/confirmacion-seguridad')}
        >
          Salir
        </button>
        <button className="btn btn-negative" onClick={redirectNextPage}>
          Inspeccionar
        </button>
      </div>
    </div>
  );
};

export default InspectOnboardingPage;
