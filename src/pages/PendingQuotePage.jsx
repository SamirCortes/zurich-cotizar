import { useNavigate } from 'react-router-dom';

//Icons
import AlertIcon from '../assets/icons/AssistancePendingQuotePage.svg';
import UserIcon from '../assets/icons/UserPendingQuotePage.svg';

const PendingQuotePage = () => {
  const navigate = useNavigate();
  const redirectNextPage = () => navigate('/');

  return (
    <div className="Page">
      <div className="PageContent">
        <div className="PageContractsContainer">
          <img src={AlertIcon} alt="" />
          <div className="PageContractsContainer__content">
            <section className="Section">
              <p className="title mb-2">
                Tienes una cotización de “Auto” pendiente
              </p>
            </section>
            <p className="PageContractsContainer__text">
              Parece que dejaste una cotización pendiente ¿Eres tú?
            </p>
            <div className="MessageBlueData">
              <img src={UserIcon} alt="" />
              <div>
                <p className="SubtitleMd">
                  Natalia Jimena Gonzales Buena Vista
                </p>
                <p className="TextLabel">RUT 20.456.987-3</p>
              </div>
            </div>
            <div className="PageActionsPendingQuote">
              <button className="btn btn-negative">Continuar cotización</button>
              <button className="btn btn-outline" onClick={redirectNextPage}>
                Empezar nueva cotización
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PendingQuotePage;
