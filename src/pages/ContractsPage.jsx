import StepForm from '../shared/StepForm/StepForm';
import { useNavigate } from 'react-router-dom';
import ContractsCards from '../shared/ContractsCards/ContractsCards';

//Icons
import IconContract from '../assets/icons/ContractSummaryPage.svg';

const ContractsPage = () => {
  const navigate = useNavigate();
  const redirectNextPage = () => navigate('/confirmacion-seguridad');

  return (
    <div className="Page">
      <StepForm step={2} title="Contrata" />
      <div className="PageContent">
        <div className="d-flex align-items-center">
          <img className="IconTitle" src={IconContract} alt="" />
          <h1>Contratos</h1>
        </div>
        <ContractsCards title={'Titulo de contrato 1'} />
        <ContractsCards title={'Titulo de contrato 2'} />
      </div>
      <div className="PageActions">
        {/* <z-button config="primary">Continuar</z-button> */}
        <button
          className="btn btn-outline"
          onClick={() => navigate('/link-pago')}
        >
          Volver
        </button>
        <button className="btn btn-negative" onClick={redirectNextPage}>
          Firmar
        </button>
      </div>
    </div>
  );
};

export default ContractsPage;
