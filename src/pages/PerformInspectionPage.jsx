import StepForm from '../shared/StepForm/StepForm';
import { useNavigate } from 'react-router-dom';
import PerfomInspectionForm from '../shared/PerfomInspectionForm/PerfomInspectionForm';

//Icons
import IconTitle from '../assets/icons/SearchInpectionPage.svg';
const PerformInspectionPage = () => {
  const navigate = useNavigate();
  return (
    <div className="Page">
      <StepForm step={3} title="Inspecciona" />
      <div className="PageContent">
        <div className="PageTop">
          <img className="IconTitle mr-2" src={IconTitle} alt="" />
          <h1>Realizar inspeccion</h1>
        </div>
        <PerfomInspectionForm />
      </div>
      <div className="PageActions">
        {/* <z-button config="primary">Continuar</z-button> */}
        <button
          className="btn btn-outline"
          onClick={() => navigate('/inspeccionar')}
        >
          Volver
        </button>
        <button
          className="btn btn-negative"
          onClick={() => navigate('/comprobante')}
        >
          Continuar
        </button>
      </div>
    </div>
  );
};

export default PerformInspectionPage;
