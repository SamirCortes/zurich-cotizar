import { useNavigate } from 'react-router-dom';
import StepForm from '../shared/StepForm/StepForm';
import HomeDataPgeForm from '../shared/HomeDataPgeForm/HomeDataPgeForm';
//Icons
import IconTitle from '../assets/icons/HomeData-title.svg';

const HomeDataPage = () => {
  const navigate = useNavigate();
  const redirectNextPage = () => navigate('/selecionar-plan');

  return (
    <div className="Page">
      <StepForm step={1} title="Cotiza" />
      <div className="PageContent">
        <div className="d-flex align-items-center">
          <img className="IconTitle" src={IconTitle} alt="" />
          <h1>Datos de tu *hogar*</h1>
        </div>
        <HomeDataPgeForm />
      </div>
      <div className="PageActions">
        {/* <z-button config="primary">Continuar</z-button> */}
        <button className="btn btn-outline" onClick={() => navigate('/')}>
          Volver
        </button>
        <button className="btn btn-negative" onClick={redirectNextPage}>
          Continuar
        </button>
      </div>
    </div>
  );
};

export default HomeDataPage;
