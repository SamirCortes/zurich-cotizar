import StepForm from '../shared/StepForm/StepForm';
import ConfigProductForm from '../shared/ConfigProductForm/ConfigProductForm';
import Coverage from '../shared/Coverages/Coverage';
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';

//Icons
import IconTitle from '../assets/icons/List_points.svg';
import IconPlus from '../assets/icons/PlanConfig_plus_.svg';

const ProductConfigPage = () => {
  const navigate = useNavigate();
  const redirectNextPage = () => navigate('/configurar-plan');
  const [coverage, setCoverage] = useState(false);

  const handleCoverage = () => setCoverage(!coverage);
  const handleCloseCoverage = () => {
    setCoverage(false);
  };
  return (
    <div className="Page">
      {coverage && <Coverage onClose={handleCloseCoverage} />}
      <StepForm step={1} title="Cotiza" />
      <div className="PageContent">
        <div className="PageTop">
          <img className="IconTitle" src={IconTitle} alt="" />
          <h1>Configurar un producto</h1>
        </div>
        <section className="Section">
          <p className="title">Título de sección uno</p>
          <button className="btn btn-primary" onClick={handleCoverage}>
            <img src={IconPlus} alt="" /> Agregar coberturas
          </button>
        </section>
        <ConfigProductForm />
      </div>
      <div className="PageActions">
        {/* <z-button config="primary">Continuar</z-button> */}
        <button
          className="btn btn-outline"
          onClick={() => navigate('/selecionar-plan')}
        >
          Volver
        </button>
        <button className="btn btn-negative" onClick={redirectNextPage}>
          Continuar
        </button>
      </div>
    </div>
  );
};

export default ProductConfigPage;
