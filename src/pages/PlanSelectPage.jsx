import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import StepForm from '../shared/StepForm/StepForm';
import Deductible from '../shared/Deductible/Deductible';
import CardPlans from '../shared/CardPlans/CardPlans';
import ApplyDiscountPlanSelect from '../shared/ApplyDiscountPlanSelect/ApplyDiscountPlanSelect'

import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/effect-coverflow';
import { EffectCoverflow } from 'swiper/modules';

//Icons
import IconTitle from '../assets/icons/PlanSelect-title.svg';
import ImgBanner from '../assets/img/PlanSelect-banner.png';

const PlanSelectPage = () => {
  const navigate = useNavigate();
  const [deductible, setDeductible] = useState('05');
  const [discount, setDiscount] = useState(false);



  const redirectNextPage = () => navigate('/configurar-producto');
  const handleDeductible = (res) => {
    setDeductible(res.target.dataset.valor);
  };
  const handleAplyDiscount = () => {
    setDiscount(true);
  };

  return (
    <div className="Page">
      <StepForm step={1} title="Cotiza" />
      <div className="PageContent">
        <div className="PageTop">
          <img className="IconTitle" src={IconTitle} alt="" />
          <h1>Elige un plan</h1>
        </div>
        <img className="Banner" src={ImgBanner} alt="" />
        <section className="Section">
          <p className="title">Selecciona deducible</p>
          <Deductible value={deductible} handleSelect={handleDeductible} />
        </section>
        <section className="Section">
          <p className="title">Selecciona un plan</p>
          <Swiper
            effect={'coverflow'}
            grabCursor={true}
            centeredSlides={true}
            slidesPerView={'auto'}
            coverflowEffect={{
              rotate: 0,
              stretch: 0,
              depth: 100,
              modifier: 1,
              slideShadows: false,
            }}
            pagination={true}
            modules={[EffectCoverflow]}
            className="mySwiper"
          >
            <SwiperSlide>
              <CardPlans valor={deductible} discount={discount} />
            </SwiperSlide>
            <SwiperSlide>
              <CardPlans valor={deductible} discount={discount} />
            </SwiperSlide>
            <SwiperSlide>
              <CardPlans valor={deductible} discount={discount} />
            </SwiperSlide>
          </Swiper>
        </section>
       <ApplyDiscountPlanSelect/>
        <section className="Section">
          <p className="title">¿Tienes un código de descuento?</p>
          <div className="codeDiscount">
            <label z-text-input="shaped">
              <span>Código</span>
              <input type="number" placeholder="" />
            </label>
            <button className="btn btn-primary" onClick={handleAplyDiscount}>
              Aplicar
            </button>
          </div>
        </section>
      </div>
      <div className="PageActions">
        {/* <z-button config="primary">Continuar</z-button> */}
        <button
          className="btn btn-outline"
          onClick={() => navigate('/datos-hogar')}
        >
          Volver
        </button>
        <button className="btn btn-negative" onClick={redirectNextPage}>
          Continuar
        </button>
      </div>
    </div>
  );
};

export default PlanSelectPage;
