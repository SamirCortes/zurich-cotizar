import StepForm from '../shared/StepForm/StepForm';
import PaymentMethodCard from '../shared/PaymentMethodCard/PaymentMethodCard';
import DataOwnerCard from '../shared/DataOwnerCard/DataOwnerCard';
import { useNavigate } from 'react-router-dom';
import Toast from '../shared/Toast/Toast';
import PagoPatCard from '../shared/PagoPatCard/PagoPatCard';

//Icons
import IconTitle from '../assets/icons/CheckVoucherPage.svg';
import IconDownload from '../assets/icons/downloadVoucherPage.svg';

const VoucherInspectionPage = () => {
  const navigate = useNavigate();
  return (
    <div className="Page">
      <StepForm step={3} title="Inspecciona" />
      <div className="PageContent mb-0">
        <div className="PageTop">
          <img className="IconTitle mr-2" src={IconTitle} alt="" />
          <h1>Comprobante</h1>
        </div>
        <section className="section mt-3">
          <p className="textParrafo">
            Te enviamos un correo a{' '}
            <span className="textParrafo__spanText">nombre@dominio.com</span>{' '}
            con el detalle
          </p>
        </section>
        <DataOwnerCard />
        <PaymentMethodCard />
        <div className="mt-3">
          <Toast
            message={
              'UF al valor del día de hoy. Tendrás 15 días para realizar la inspección de tu auto para que  tu póliza sea vigente.'
            }
          />
        </div>
        <PagoPatCard />
        <div className='downloadVoucher'>
          <img src={IconDownload} alt="" />
          Descargar comprobante
        </div>
        <section className="Section">
          <button className="btn btn-primary" onClick={() => navigate('/')}>
            Volvel a la pagina principal
          </button>
        </section>
      </div>
    </div>
  );
};

export default VoucherInspectionPage;
