import './App.scss';
import { Outlet } from 'react-router-dom';
import { lazy, Suspense } from 'react';
import { HashRouter, useRoutes } from 'react-router-dom';
import NavBar from './shared/NavBar/NavBar';

const LazyLoad = ({ Comp }) => {
  return (
    <Suspense fallback={<div>loading....</div>}>
      <Comp />
    </Suspense>
  );
};

// Pages
let GeneralDataPage = lazy(() => import('./pages/GeneralDataPage'));
let HomeDataPage = lazy(() => import('./pages/HomeDataPage'));
let PlanSelectPage = lazy(() => import('./pages/PlanSelectPage'));
let ProductConfigPage = lazy(() => import('./pages/ProductConfigPage'));
let PaymentMethodPage = lazy(() => import('./pages/PaymentMethodPage'));
let PlanConfigPage = lazy(() => import('./pages/PlanConfigPage'));
let MeanspaymentPage = lazy(() => import('./pages/MeanspaymentPage'));
let OnboardingContractsPage = lazy(() =>
  import('./pages/OnboardingContractsPage')
);
let PaymentLinkPage = lazy(() => import('./pages/PaymentLinkPage'));
let ClientStatusPage = lazy(() => import('./pages/ClientStatusPage'));
let ContractsPage = lazy(() => import('./pages/ContractsPage'));
let SecurityConfirmationPage = lazy(() =>
  import('./pages/SecurityConfirmationPage')
);
let InspectOnboardingPage = lazy(() => import('./pages/InspectOnboardingPage'));
let PerformInspectionPage = lazy(() => import('./pages/PerformInspectionPage'));
let VoucherInspectionPage = lazy(() => import('./pages/VoucherInspectionPage'));
let PendingQuotePage = lazy(() => import('./pages/PendingQuotePage'));

const Router = () => {
  const r = useRoutes([
    {
      path: '/',
      element: <LazyLoad Comp={GeneralDataPage} />,
    },
    {
      path: '/datos-generales',
      element: <LazyLoad Comp={GeneralDataPage} />,
    },
    {
      path: '/datos-hogar',
      element: <LazyLoad Comp={HomeDataPage} />,
    },
    {
      path: '/selecionar-plan',
      element: <LazyLoad Comp={PlanSelectPage} />,
    },
    {
      path: '/configurar-producto',
      element: <LazyLoad Comp={ProductConfigPage} />,
    },
    {
      path: '/configurar-plan',
      element: <LazyLoad Comp={PlanConfigPage} />,
    },
    {
      path: '/modalidad-pago',
      element: <LazyLoad Comp={PaymentMethodPage} />,
    },
    {
      path: '/medios-pago',
      element: <LazyLoad Comp={MeanspaymentPage} />,
    },
    {
      path: '/contrato',
      element: <LazyLoad Comp={OnboardingContractsPage} />,
    },
    {
      path: '/link-pago',
      element: <LazyLoad Comp={PaymentLinkPage} />,
    },
    {
      path: '/estado-cliente',
      element: <LazyLoad Comp={ClientStatusPage} />,
    },
    {
      path: '/contratos',
      element: <LazyLoad Comp={ContractsPage} />,
    },
    {
      path: '/confirmacion-seguridad',
      element: <LazyLoad Comp={SecurityConfirmationPage} />,
    },
    {
      path: '/inspeccionar',
      element: <LazyLoad Comp={InspectOnboardingPage} />,
    },
    {
      path: '/realizar-inspeccion',
      element: <LazyLoad Comp={PerformInspectionPage} />,
    },
    {
      path: '/comprobante',
      element: <LazyLoad Comp={VoucherInspectionPage} />,
    },
    {
      path: '/cotizacion-pendiente',
      element: <LazyLoad Comp={PendingQuotePage} />,
    },
  ]);

  return r;
};

export default function App() {
  return (
    <HashRouter>
      <div className="App">
        <NavBar />

        <Outlet />

        <Router />
      </div>
    </HashRouter>
  );
}
