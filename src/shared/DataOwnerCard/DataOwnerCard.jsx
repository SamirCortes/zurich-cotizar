import './DataOwnerCard.scss';
//Icons
import IconCardTitle from '../../assets/icons/EmployeeDataOwner.svg';

function DataOwnerCard() {
  return (
    <div className="DataOwnerCard">
      <div className="DataOwnerCard__header mt-4">
        <img src={IconCardTitle} alt="" />
        <div className="DataOwnerCard__header__content">
          <p className="DataOwnerCard__header__title">Propietario y pagador</p>
          <div className="DataOwnerCard__header__contentPrices">
            <p className="DataOwnerCard__header__price">
              Natalia Jimena Gonzales Buena Vista
            </p>
          </div>
        </div>
      </div>
      <div className="DataOwnerCard__content">
        <div className="DataOwnerCard__content__item">
          <p>Rut</p>
          <p className="DataOwnerCard__content__item__attribute">
            20.456.987-3
          </p>
        </div>
        <div className="DataOwnerCard__content__item">
          <p>Dirección</p>
          <p className="DataOwnerCard__content__item__attribute">
            Calle siempre viva 742 - Casa 11
          </p>
        </div>
        <div className="DataOwnerCard__content__item">
          <p>Comuna</p>
          <p className="DataOwnerCard__content__item__attribute">Las condes</p>
        </div>
        <div className="DataOwnerCard__content__item">
          <p>Email</p>
          <p className="DataOwnerCard__content__item__attribute">
            njgbuenavista@correo.com
          </p>
        </div>
        <div className="DataOwnerCard__content__item">
          <p>Teelfono</p>
          <p className="DataOwnerCard__content__item__attribute">
            +56 9 77 123 654
          </p>
        </div>
      </div>
    </div>
  );
}

export default DataOwnerCard;
