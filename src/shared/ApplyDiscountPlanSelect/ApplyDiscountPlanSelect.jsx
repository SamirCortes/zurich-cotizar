import './ApplyDiscountPlanSelect.scss';

import Toast from '../../shared/Toast/Toast';

const ApplyDiscountPlanSelect = () => {
  return (
    <section className="Section">
      <div className="contentForm__radioSelect">
        <p className="Subtitle">Aplicar descuentos o recargos</p>
        <label z-checkbox className="d-flex">
          <input type="checkbox" />
          <span className="TextLabel ml-3">Tengo un descuento promocional</span>
        </label>
        <div className="codeDiscount ml-3">
          <label z-text-input="shaped">
            <span>Código</span>
            <input type="number" placeholder="" />
          </label>
          <button className="btn btn-primary">Aplicar</button>
        </div>
        <label z-checkbox className="d-flex">
          <input type="checkbox" />
          <span className="TextLabel  ml-3">Descuento de comisión </span>
        </label>
        <z-select
          value="a"
          config="shaped"
          label="Porcentaje"
          options='[{"text":"10% - Comisión $ 90.000","value":"a"},{"text":"Option B","value":"b"},{"text":"Option C","value":"c"},{"text":"Option D","value":"d"}]'
        />
        <label z-checkbox className="d-flex mt-3">
          <input type="checkbox" disabled />
          <span className="TextLabel  ml-3">Recargo sobre la prima </span>
        </label>
        <label z-checkbox className="d-flex">
          <input type="checkbox" />
          <span className="TextLabel  ml-3">Descuento sobre la prima </span>
        </label>
        <z-select
          value="a"
          config="shaped"
          label="Porcentaje"
          options='[{"text":"5% - Comisión $ 50.000","value":"a"},{"text":"6% - Comisión $ 60.000","value":"b"},{"text":"7% - Comisión $ 70.000","value":"c"},{"text":"8% - Comisión $ 60.000 ","value":"d"},{"text":"10% - Comisión $ 100.000 ","value":"e"}]'
        />
      </div>
      <Toast message={'Debes seleccionar un plan para poder continuar.'} />
    </section>
  );
};

export default ApplyDiscountPlanSelect;
