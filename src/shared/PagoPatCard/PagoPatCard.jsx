import './PagoPatCard.scss';
//Icons
import IconCardTitle from '../../assets/icons/CashPagoPatCard.svg';
import IconCardCheck from '../../assets/icons/PaymentMethodCard-iconCheck.svg';

function PagoPatCard() {
  return (
    <div className="PagoPatCard">
      <div className="PagoPatCard__tag">
        <img src={IconCardCheck} alt="" />
        Pago PAT
      </div>
      <div className="PagoPatCard__header">
        <img src={IconCardTitle} alt="" />
        <div className="PagoPatCard__header__content">
          <p className="PagoPatCard__header__title">Vence 15 de cada mes</p>
          <div className="PagoPatCard__header__contentPrices">
            <p className="PagoPatCard__header__price">Visa****7890</p>
          </div>
        </div>
      </div>
      <div className="PagoPatCard__content">
        <div className="PagoPatCard__content__item">
          <p>Valor 1° cuota</p>
          <p className="PagoPatCard__content__item__attribute">
           $68.465<span className='PagoPatCard__content__item__spann'>(UF5)</span>
          </p>
        </div>
        <div className="PagoPatCard__content__item">
          <p>Vencimiento</p>
          <p className="PagoPatCard__content__item__attribute">
           15 Abr 2024
          </p>
        </div>
      </div>
    </div>
  );
}

export default PagoPatCard;
