import './CoverageGroup.scss';

//Icons
import IconClose from '../../assets/icons/PlanConfigCloseBlueSolid.svg';

const CoverageGroup = () => {
  return (
    <section className="Section">
      <p className="title">Coberturas "titulo de grupo 1"</p>
      <div className="OptionsContent">
        <div className="OptionsContent__Title">
          <label className="OptionsContent__Label">Option 1A</label>
          <img src={IconClose} alt="" />
        </div>
      </div>
      <div className="OptionsContent">
        <div className="OptionsContent__Title">
          <label className="OptionsContent__Label">Option 1B</label>
          <img src={IconClose} alt="" />
        </div>
        <div className="OptionsContent__Input">
          <label z-text-input="shaped">
            <span>Desplegable</span>
            <input type="text" placeholder="" />
          </label>
          <label z-text-input="shaped" className='w-50'>
            <span>Input corto</span>
            <input type="text" placeholder="" />
          </label>
        </div>
      </div>
    </section>
  );
};

export default CoverageGroup;
