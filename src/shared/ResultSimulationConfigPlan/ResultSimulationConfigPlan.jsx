import './ResultSimulationConfigPlan.scss';

//Icons
import IconDownload from '../../assets/icons/DocumentDownload.svg';
import IconBack from '../../assets/icons/ArrowBack_ResultSimulation.svg';
import IconShare from '../../assets/icons/Share_ResulSimulation.svg';

const ResultSimulationConfigPlan = ({ valor, onClose }) => {
  return (
    <div className="ResultSimulation">
      <p className="titlemd">Resultado de la simulación</p>
      <div className="ResultSimulationConfigPlan">
        <div className="ResultSimulationConfigPlan__header">
          <p className="ResultSimulationConfigPlan__header__time">mensual</p>
          <p className="ResultSimulationConfigPlan__header__type">$ 000.000</p>
          <p className="ResultSimulationConfigPlan__header__value">
            UF {valor}
          </p>
        </div>
        <div className="ResultSimulationConfigPlan__content">
          <div className="ResultSimulationConfigPlan__content__item">
            <p>Atributo uno</p>
            <p className="ResultSimulationConfigPlan__content__item__attribute">
              Detalle de atributo
            </p>
          </div>
          <div className="ResultSimulationConfigPlan__content__item">
            <p>Atributo uno</p>
            <p className="ResultSimulationConfigPlan__content__item__attribute">
              Detalle de atributo
            </p>
          </div>
          <div className="ResultSimulationConfigPlan__content__item">
            <p>Atributo uno</p>
            <p className="ResultSimulationConfigPlan__content__item__attribute">
              Detalle de atributo
            </p>
          </div>
          <div className="ResultSimulationConfigPlan__content__item">
            <p>Atributo uno</p>
            <p className="ResultSimulationConfigPlan__content__item__attribute">
              Detalle de atributo
            </p>
          </div>
          <div className="ResultSimulationConfigPlan__content__item">
            <p>Atributo uno</p>
            <p className="ResultSimulationConfigPlan__content__item__attribute">
              Detalle de atributo
            </p>
          </div>
        </div>

        <div className="ResultSimulationConfigPlan__content">
          <div className="d-flex">
            <img src={IconDownload} alt="" />
            <p className="SubtitleLink">Ver PDF completo</p>
          </div>
        </div>
      </div>
      <div className="options">
        <div className='options__back' onClick={onClose}>
          <img src={IconBack} alt="" />
          <p className='SubtitleLink ml-2'> Volver a simular</p>
        </div>
        <div className='options__share'>
          <img src={IconShare} alt="" />
          <p className='SubtitleLink ml-2'>Compartir simulación</p>
        </div>
      </div>
    </div>
  );
};

export default ResultSimulationConfigPlan;
