import './HomeDataPgeForm.scss';

const HomeDataPgeForm = () => {
  return (
    <>
      <section className="Section">
        <p className="title">Título de sección 1</p>
        <div className="contentForm">
          <label z-date-input="shaped">
            <span>Fecha de nacimiento</span>
            <input type="date" />
          </label>
          <label z-date-input="shaped">
            <span>Fecha de nacimiento</span>
            <input type="date" />
          </label>

          <label z-date-input="shaped" className="w-75">
            <span>Fecha de nacimiento</span>
            <small className="mt-1">
              <strong>Debe ser mayor de 18 años</strong>
            </small>
            <input type="date" />
          </label>
          <div className="contentForm__radioSelect">
            <p className="Subtitle">Pregunta con opcíon multiple</p>
            <z-radio-select
              className="d-flex gap-4"
              value="Opcíon 1"
              name="radio-select"
              options='[{ "text": "Opcíon 1", "value": "Opcíon 1"}, { "text": "Opcíon 2", "value": "Opcíon 2"}, { "text": "Opcíon 3", "value": "Opcíon 3"}, { "text": "Opcíon 4", "value": "Opcíon 1"}]'
            ></z-radio-select>
          </div>
        </div>
      </section>
      <section className="Section">
        <p className="title">Título de sección 2</p>
        <div className="contentForm">
          <label z-text-input="shaped">
            <span>input simple</span>
            <input type="text" placeholder="" />
          </label>
          <label z-text-input="shaped">
            <span>input predictivo</span>
            <input type="text" placeholder="" />
          </label>

          <label z-date-input="shaped" className="w-75">
            <span>Rango de fecha</span>
            <input type="date" />
          </label>
          <div className="contentForm__radioSelect">
            <p className="Subtitle">Pregunta multi-respuesta</p>
            <label z-checkbox className="d-flex">
              <input type="checkbox" />
              <span className='TextLabel ml-3'>Opcíon 1</span>
            </label>
            <label z-checkbox className="d-flex">
              <input type="checkbox" />
              <span className='TextLabel  ml-3'>Opcíon 2 </span>
            </label>
            <label z-checkbox className="d-flex">
              <input type="checkbox" />
              <span className='TextLabel  ml-3'>Opcíon 3 </span>
            </label>
            <label z-checkbox className="d-flex">
              <input type="checkbox" />
              <span className='TextLabel  ml-3'>Opcíon 4 </span>
            </label>
          </div>
        </div>
      </section>
    </>
  );
};

export default HomeDataPgeForm;
