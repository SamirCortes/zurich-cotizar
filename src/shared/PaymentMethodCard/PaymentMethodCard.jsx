import "./PaymentMethodCard.scss";
//Icons
import IconCardTitle from "../../assets/icons/PaymentMethodCard-title.svg";
import IconCardCheck from "../../assets/icons/PaymentMethodCard-iconCheck.svg";

function PaymentMethodCard() {
  return (
    <div className="PaymentMethodCard">
      <div className="PaymentMethodCard__tag">
        <img src={IconCardCheck} alt="" />
        Valor póliza mensual
      </div>
      <div className="PaymentMethodCard__header">
        <img src={IconCardTitle} alt="" />
        <div className="PaymentMethodCard__header__content">
          <p className="PaymentMethodCard__header__title">Plan completo</p>
          <div className="PaymentMethodCard__header__contentPrices">
            <p className="PaymentMethodCard__header__price">$ 68.465</p>
            <p className="PaymentMethodCard__header__deductible">(UF 1,92)</p>
          </div>
        </div>
      </div>
      <div className="PaymentMethodCard__content">
        <div className="PaymentMethodCard__content__item">
          <p>Atributo uno</p>
          <p className="PaymentMethodCard__content__item__attribute">
            Detalle de atributo
          </p>
        </div>
        <div className="PaymentMethodCard__content__item">
          <p>Atributo uno</p>
          <p className="PaymentMethodCard__content__item__attribute">
            Detalle de atributo
          </p>
        </div>
        <div className="PaymentMethodCard__content__item">
          <p>Atributo uno</p>
          <p className="PaymentMethodCard__content__item__attribute">
            Detalle de atributo
          </p>
        </div>
        <div className="PaymentMethodCard__content__item mt-3">
          <p>Atributo uno</p>
          <p className="PaymentMethodCard__content__item__attribute">
            Detalle de atributo
          </p>
        </div>
        <div className="PaymentMethodCard__content__item">
          <p>Atributo uno</p>
          <p className="PaymentMethodCard__content__item__attribute">
            Detalle de atributo
          </p>
        </div>
        <div className="PaymentMethodCard__content__item">
          <p>Atributo uno</p>
          <p className="PaymentMethodCard__content__item__attribute">
            Detalle de atributo
          </p>
        </div>
      </div>
    </div>
  );
}

export default PaymentMethodCard;
