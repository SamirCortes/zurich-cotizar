import './StepForm.scss';
//Icons
import IconStepper1 from '../../assets/icons/Stepper-1.svg';
import IconStepper2 from '../../assets/icons/Stepper-2.svg';
import IconStepper3 from '../../assets/icons/Stepper-3.svg';

function StepForm({ step, title }) {
  return (
    <div className="StepForm">
      <img
        className="iconStep"
        src={
          step === 1 ? IconStepper1 : step === 2 ? IconStepper2 : IconStepper3
        }
        alt=""
      />
      <p
        className={
          step === 1
            ? 'justify-content-start'
            : step === 2
            ? 'justify-content-center'
            : 'justify-content-end'
        }
      >
        #{step}
        <span>{title}</span>
      </p>
    </div>
  );
}

export default StepForm;
