import "./Deductible.scss";

function Deductible({ value, handleSelect }) {
  const listDeductible = ["05", "10", "15", "20"];
  return (
    <div className="Deductible">
      {listDeductible.map((res, index) => (
        <div
          className={
            value === res ? "Deductible__item--active" : "Deductible__item"
          }
          key={index}
          onClick={handleSelect}
          data-valor={res}
        >
          UF {res}
        </div>
      ))}
    </div>
  );
}

export default Deductible;
