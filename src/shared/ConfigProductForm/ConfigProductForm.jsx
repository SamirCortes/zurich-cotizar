import './ConfigProductForm.scss';
import { useState } from 'react';
import ResultSimulationConfigPlan from '../ResultSimulationConfigPlan/ResultSimulationConfigPlan';

const ConfigProductForm = () => {
  const [resultSimulation, setResulSimulation] = useState(false);
  const handleResultSimulation = () => setResulSimulation(!resultSimulation);
  const handleCloseResultSimulaton = () => setResulSimulation(false);
  return (
    <>
      <div className="FormContent">
        <section className="Section mt-2">
          <p className="title">Título de sección dos</p>
          <p className="TextLabel">Con texto descriptivo.</p>
        </section>

        <div className="FormProduct">
          <div className="contentForm__radioSelect">
            <p className="Subtitle">Pregunta multi-respuesta</p>
            <label z-checkbox className="d-flex">
              <input type="checkbox" />
              <span className="TextLabel ml-3">Opción simple</span>
            </label>
            <label z-checkbox className="d-flex">
              <input type="checkbox" />
              <span className="TextLabel  ml-3">Opción simple</span>
            </label>
            <label z-checkbox className="d-flex">
              <input type="checkbox" />
              <span className="TextLabel  ml-3">Opción con descripción </span>
            </label>
            <label z-checkbox className="d-flex">
              <input type="checkbox" />
              <span className="TextLabel  ml-3">Descuento sobre la prima </span>
            </label>
          </div>
        </div>

        <label className="Subtitle mt-3">Pregunta multi-respuesta</label>
        <div className="FormProduct">
          <z-radio-select
            className="d-flex gap-4"
            value="Opcíon 1"
            name="radio-select"
            options='[{ "text": "Opción simple marcada por defaul", "value": "Opcíon 1"},  { "text": "Opción con descripción", "value": "Opcíon 1"},  { "text": "Opción cón acción", "value": "Opcíon 1"}]'
          ></z-radio-select>
        </div>
        <section className="Section">
          <p className="title">Título de sección tres</p>
          <div className="codeDiscount">
            <label z-text-input="shaped">
              <span>Código</span>
              <input type="number" placeholder="" />
            </label>
            <button className="btn btn-primary">Aplicar</button>
          </div>
        </section>
        {!resultSimulation && (
          <button
            class="btn btn-negative mt-4 w-50"
            onClick={handleResultSimulation}
          >
            Calcular
          </button>
        )}
      </div>
      {resultSimulation && (
        <ResultSimulationConfigPlan onClose={handleCloseResultSimulaton} />
      )}
    </>
  );
};

export default ConfigProductForm;
