import './ContractsCards.scss';

//Icons
import ArowUpIcon from '../../assets/icons/ArrowUpContractsCard.svg'

const ContractsCards = ({title}) => {
  return (
    <div className="ContractsCard">
      <div className="ContractsCard__header">
      <input z-checkbox type="checkbox"/>
        <p className="SubtitleMd mr-5">{title}</p>
        <img src={ArowUpIcon} alt="" />
      </div>
      <div className="ContractsCard__content">
        <div className="ContractsCard__content__item">
          <p>Atributo uno</p>
          <p className="ContractsCard__content__item__attribute">
            Detalle de atributo
          </p>
        </div>
        <div className="ContractsCard__content__item">
          <p>Atributo uno</p>
          <p className="ContractsCard__content__item__attribute">
            Detalle de atributo
          </p>
        </div>
        <div className="ContractsCard__content__item">
          <p>Atributo uno</p>
          <p className="ContractsCard__content__item__attribute">
            Detalle de atributo
          </p>
        </div>
        <div className="ContractsCard__content__item">
          <p>Atributo uno</p>
          <p className="ContractsCard__content__item__attribute">
            Detalle de atributo
          </p>
        </div>
        <div className="ContractsCard__content__item">
          <p>Atributo uno</p>
          <p className="ContractsCard__content__item__attribute">
            Detalle de atributo
          </p>
        </div>
        <div className="ContractsCard__content__item">
          <p>Atributo uno</p>
          <p className="ContractsCard__content__item__attribute">
            Detalle de atributo
          </p>
        </div>
        <div className="ContractsCard__content__showMore">
          <p className='SubtitleLink'>Leer contrato completo</p>
         
        </div>
      </div>
    </div>
  );
};

export default ContractsCards;
