import './Coverage.scss';
import React from 'react';
import Iconclose from '../../assets/icons/CloseCoverage.svg';

const Coverage = (props) => {
  return (
    <div className="coverageContainer">
      <div className="PageContent coverageContainer__content pb-0">
        <div className="d-flex justify-content-end mb-2">
          <img src={Iconclose} alt="" onClick={props.onClose} />
        </div>
        <section className="Section mt-0">
          <h1 className="m-0">Coberturas</h1>
          <p className="title">Título de grupo 1</p>
          <div className="FormProduct">
            <div className="contentForm__radioSelect">
              <label z-checkbox className="d-flex">
                <input type="checkbox" />
                <span className="TextLabel ml-3">
                  Opción 1A incluida por default
                </span>
              </label>
              <label z-checkbox className="d-flex">
                <input type="checkbox" />
                <span className="TextLabel  ml-3">Opción 1B</span>
              </label>
              <label z-checkbox className="d-flex">
                <input type="checkbox" />
                <span className="TextLabel  ml-3">Opción 1C </span>
              </label>
              <label z-checkbox className="d-flex">
                <input type="checkbox" />
                <span className="TextLabel  ml-3">Opción 1D</span>
              </label>
            </div>
            <p className="title">Título de grupo 2</p>
            <div className="contentForm__radioSelect">
              <label z-checkbox className="d-flex">
                <input type="checkbox" />
                <span className="TextLabel ml-3">
                  Opción 2A
                </span>
              </label>
              <label z-checkbox className="d-flex">
                <input type="checkbox" />
                <span className="TextLabel  ml-3">Opción 2B</span>
              </label>
              <label z-checkbox className="d-flex">
                <input type="checkbox" />
                <span className="TextLabel  ml-3">Opción 2C </span>
              </label>
              <label z-checkbox className="d-flex">
                <input type="checkbox" />
                <span className="TextLabel  ml-3">Opción 2D</span>
              </label>
              <label z-checkbox className="d-flex">
                <input type="checkbox" />
                <span className="TextLabel  ml-3">Opción 2E</span>
              </label>
              <label z-checkbox className="d-flex">
                <input type="checkbox" />
                <span className="TextLabel  ml-3">Opción 2F</span>
              </label>
              <label z-checkbox className="d-flex">
                <input type="checkbox" />
                <span className="TextLabel  ml-3">Opción 2G</span>
              </label>
            </div>
          </div>
        </section>
        <div className="PageActions">
          <button className="btn btn-negative">Aplicar</button>
        </div>
      </div>
    </div>
  );
};

export default Coverage;
