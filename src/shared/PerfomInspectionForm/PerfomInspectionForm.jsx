import './PerfomInspectionForm.scss';

//Icons
import IconQuestion from '../../assets/icons/QuestionInspectForm.svg';
import IconPhone from '../../assets/icons/PhoneInspectionForm.svg';

const PerfomInspectionForm = () => {
  return (
    <>
      <section className="Section">
        <p className="title">Pregunta binaria</p>
        <z-radio-select
          config="inline"
          value="Si"
          name="radio-select"
          options='[{ "text": "Si", "value": "Si"}, { "text": "No", "value": "No"}]'
        ></z-radio-select>
      </section>
      <section className="Section">
        <div className="message">
          <img src={IconPhone} alt="" />
          <p className='message__TextMessage'>
            Te enviaremos un <span>link a tu teléfono</span>, sólo tendrás que sacar unas
            fotos de acuerdo a las indicaciones que iremos entregando.<strong> Tienes
            48hs para realizarla.</strong>
          </p>
        </div>
      </section>
      <section className="Section">
        <p className="titleForm">Verifica tus datos</p>
        <label z-text-input="shaped" icon="mail-closed:outline">
          <span>Email</span>
          <input type="text" placeholder="" />
        </label>
        <label z-number-input="shaped" icon="phone-closed:outline">
          <span>Teléfono</span>
          <small>Cód.país + Cód.area + Número (Sin el 15)</small>
          <input type="number" placeholder="+56923440092" />
        </label>
      </section>
      <section className="Section">
        <div className="d-flex justify-content-between">
          <p className="title">¿Quiere solicitar un dispositivo GPS?</p>
          <img src={IconQuestion} alt="" />
        </div>

        <label className="textLabel">Puede incluir costos extra.</label>
        <z-radio-select
          config="inline"
          value="Si"
          name="radio-select"
          options='[{ "text": "Si", "value": "Si"}, { "text": "No lo quiero por ahora", "value": "No"}]'
        ></z-radio-select>
      </section>
    </>
  );
};

export default PerfomInspectionForm;
