import './SureExtraPaymentMethodPage.scss';
import React, { useState } from 'react';

//Icons
import iconArrow from '../../assets/icons/ArrowDown.svg';
const SureExtraPaymentMethodPage = () => {
  const [show, setShow] = useState(false);
  const handleShow = () => setShow(!show);
  return (
    <div className="SureExtraPaymentMethodPage">
      <div className="SureExtraPaymentMethodPage__header">
        <input z-checkbox type="checkbox" />
        <p>
          ¿Quieres sumar <strong>*seguro extra*</strong> por $ 00.000?
        </p>
      </div>
      <div className={!show ? 'd-none' : ''}>
        <div className="SureExtraPaymentMethodPage__content__title">
          <p>Titulo</p>
        </div>

        <div className="SureExtraPaymentMethodPage__content">
          <div className="SureExtraPaymentMethodPage__content__item">
            <p>Atributo uno</p>
            <p className="SureExtraPaymentMethodPage__content__item__attribute">
              Detalle de atributo
            </p>
          </div>
          <div className="SureExtraPaymentMethodPage__content__item">
            <p>Atributo uno</p>
            <p className="SureExtraPaymentMethodPage__content__item__attribute">
              Detalle de atributo
            </p>
          </div>
          <div className="SureExtraPaymentMethodPage__content__item">
            <p>Atributo uno</p>
            <p className="SureExtraPaymentMethodPage__content__item__attribute">
              Detalle de atributo
            </p>
          </div>
          <div className="SureExtraPaymentMethodPage__content__item">
            <p>Atributo uno</p>
            <p className="SureExtraPaymentMethodPage__content__item__attribute">
              Detalle de atributo
            </p>
          </div>
          <div className="SureExtraPaymentMethodPage__content__item">
            <p>Atributo uno</p>
            <p className="SureExtraPaymentMethodPage__content__item__attribute">
              Detalle de atributo
            </p>
          </div>
          <div className="SureExtraPaymentMethodPage__content__item">
            <p>Atributo uno</p>
            <p className="SureExtraPaymentMethodPage__content__item__attribute">
              Detalle de atributo
            </p>
          </div>
          <div className="contentValue">
            <p className="contentValue__title">Valor póliza mensual</p>
            <p className="contentValue__value">
              $000.000 <span>UF 0,80</span>
            </p>
          </div>
        </div>
      </div>{' '}
      <div className="SureExtraPaymentMethodPage__content">
          <p onClick={() => handleShow(show)} className="SubtitleLink">
          {show ? 'Ocultar características' : 'Vercaracterísticas '}
          <img  className={show ? 'arrowPosition' : ''}  src={iconArrow} alt="" />
        </p>
      </div>
    </div>
  );
};

export default SureExtraPaymentMethodPage;
