import './NavBar.scss';
import logoZurich from '../../assets/icons/Zurich_Logo_Horz_White_RGB.svg';
import logoBanco from '../../assets/icons/Banco_Santander_Logotipo 1.svg';
import iconArrow from '../../assets/icons/Arrow_Left.svg';
import iconMenu from '../../assets/icons/Menu_Blue_Outline_RGB.svg';
import iconPhone from '../../assets/icons/Phone_Blue_Outline_RGB (1).svg';

function NavBar() {
  return (
    <nav>
      <div className="container">
        <img src={iconArrow} alt=""  />
        <div className="container-logos">
          <img src={logoZurich} alt="" />
          <img src={logoBanco} alt="" className='mb-1'/>
        </div>
        <img src={iconMenu} alt="" />
      </div>
      <div className="container-contacto w-246 h-24">
        <div className="contacto">
          <img src={iconPhone} alt="" />
          <span className="text-white">Contacto</span>
        </div>
        <div className="menu">
          <img src={iconMenu} alt="" />
          <span className="text-white">Menú</span>
        </div>
      </div>
    </nav>
  );
}

export default NavBar;
