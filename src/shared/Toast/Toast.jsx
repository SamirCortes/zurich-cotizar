import React from 'react';
import './Toast.scss';
import checIcon from '../../assets/icons/CheckGreenToast.svg'

const Toast = ({ title, message }) => {
  return (
    <div className="Toast mt-4">
      <img src={checIcon} alt="" />  
      <div className='ml-3'>
        <span className="Toast__title">{title}</span>
        <p className="Toast__message">{message}</p>
      </div>
    </div>
  );
};

export default Toast;
