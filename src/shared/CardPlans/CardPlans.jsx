import './CardPlans.scss';
import React, { useState } from 'react';


//Icons
import IconArrowDown from '../../assets/icons/CardPlans-seeMore.svg';
import IconShared from '../../assets/icons/CardPlans-shared.svg';
import IconRibbonLeft from '../../assets/icons/CardPlans-ribbon-left.svg';
import IconRibbonRight from '../../assets/icons/CardPlans-ribbon-right.svg';

function CardPlans({ valor, discount }) {
  const [select, setSelect] = useState(false);

  const handleSelect = () => setSelect(true);
  return (
    <>
      {discount && (
        <div className="DiscountAlert">¡Código aplicado! {valor}% OFF</div>
      )}
      <div className="CardPlans">
        <div className="CardPlans__recommended">Recomendado</div>
        <div className="CardPlans__header">
          <p className="CardPlans__header__time">mensual</p>
          <p className="CardPlans__header__type">Deducible</p>
          <p className="CardPlans__header__value">UF {valor}</p>
        </div>
        <div className="CardPlans__content">
          <p className="CardPlans__content__name">Artículo uno</p>
          <p className="CardPlans__content__detail">Detalle de artículo</p>
          <p className="CardPlans__content__name">Artículo uno</p>
          <p className="CardPlans__content__detail">Detalle de artículo</p>
          <p className="CardPlans__content__name">Artículo uno</p>
          <p className="CardPlans__content__detail">Detalle de artículo</p>

          <p className="CardPlans__content__name">Artículo uno</p>
          <p className="CardPlans__content__detail">Detalle de artículo</p>
          <div className="CardPlans__content__seeMore">
            Ver más <img className="icon" src={IconArrowDown} alt="" />
          </div>
        </div>
        <div className="CardPlans__footer">
          <p className="CardPlans__footer__name">Valor póliza mensual</p>
          {discount && (
            <p className="CardPlans__footer__valueDiscount">$ 000.000</p>
          )}
          <p className="CardPlans__footer__value">
            $000.000 {discount && <p className="discount">5% OFF</p>}
          </p>
          <p className="CardPlans__footer__valueType">UF 0,80</p>
          {discount && (
            <div className="CardPlans__footer__ribbon">
              <img className="icon left" src={IconRibbonLeft} alt="" />
              ¡1° Mes sin cargo!
              <img className="icon right" src={IconRibbonRight} alt="" />
            </div>
          )}
          <div
            className={
              !select
                ? 'CardPlans__footer__selected'
                : 'CardPlans__footer__selected CardPlans__footer__selected--active'
            }
            onClick={handleSelect}
          >
            <z-radio-select
              value=""
              name="radio-select"
              options='[{ "text": "Selecconar", "value": "Opcíon 1"}]'
            ></z-radio-select>
          </div>
        </div>
      </div>
      <div className="SharedPlan">
        <img className="icon" src={IconShared} alt="" />
        Compartir planes
      </div>
     
    </>
  );
}

export default CardPlans;
